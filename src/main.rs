use std::sync::Mutex;

use rand::distributions::WeightedIndex;
use rand::prelude::Distribution;
use tower_lsp::jsonrpc::Result;
use tower_lsp::lsp_types::*;
use tower_lsp::{Client, LanguageServer, LspService, Server};

#[derive(Debug)]
struct Backend {
    client: Client,
    current_response: Mutex<String>,
}

struct Resp<'a> {
    msg: &'a str,
    weight: usize,
}

const DOCUMENTATION: &str = r"
    Does nothing.
    Wants for nothing.
    Needs nothing.
";
const RESPONSES: &[Resp] = &[
    Resp {
        msg: "Syntax Error: Unexpected text",
        weight: 50,
    },
    Resp {
        msg: "No!",
        weight: 20,
    },
    Resp {
        msg: "Bro it's not that hard, quit typing",
        weight: 1,
    },
];

fn generate_response() -> String {
    let weights = RESPONSES.iter().map(|r| r.weight);
    let weight_dist = WeightedIndex::new(weights).unwrap();
    let index = weight_dist.sample(&mut rand::thread_rng());
    return RESPONSES[index].msg.to_owned();
}

fn trigger_chars() -> Vec<String> {
    let mut out: Vec<String> = Vec::new();
    for i in 0..=255u8 {
        out.push((i as char).to_string());
    }
    return out;
}

impl Backend {
    async fn judgement(&self, text: &str, uri: Url) {
        if text == "" {
            let mut current_response = self.current_response.lock().unwrap();
            *current_response = generate_response();
            self.client.publish_diagnostics(uri, Vec::new(), None)
        } else {
            let current_response = self.current_response.lock().unwrap();
            let end_position = Position::new(u32::MAX, u32::MAX);
            self.client.publish_diagnostics(
                uri,
                vec![Diagnostic {
                    range: Range::new(Position::new(0, 0), end_position),
                    severity: Some(DiagnosticSeverity::ERROR),
                    code: Some(NumberOrString::Number(1)),
                    code_description: None,
                    source: None,
                    message: current_response.to_string(),
                    related_information: None,
                    tags: None,
                    data: None,
                }],
                None,
            )
        }
        .await;
    }
}

#[tower_lsp::async_trait]
impl LanguageServer for Backend {
    async fn initialize(&self, _: InitializeParams) -> Result<InitializeResult> {
        Ok(InitializeResult {
            capabilities: ServerCapabilities {
                text_document_sync: Some(TextDocumentSyncCapability::Kind(
                    TextDocumentSyncKind::FULL,
                )),
                position_encoding: Some(PositionEncodingKind::UTF8),
                completion_provider: Some(CompletionOptions {
                    resolve_provider: Some(false),
                    trigger_characters: Some(trigger_chars()),
                    all_commit_characters: None,
                    work_done_progress_options: WorkDoneProgressOptions {
                        work_done_progress: None,
                    },
                    completion_item: None,
                }),
                ..Default::default()
            },
            server_info: Some(ServerInfo {
                name: "no-analyzer".to_owned(),
                version: Some("1.0.0".to_owned()),
            }),
        })
    }

    async fn initialized(&self, _: InitializedParams) {
        self.client
            .log_message(MessageType::INFO, "server initialized!")
            .await;
    }

    async fn did_open(&self, p: DidOpenTextDocumentParams) {
        let text = p.text_document.text;
        self.judgement(&text, p.text_document.uri).await;
    }

    async fn did_change(&self, p: DidChangeTextDocumentParams) {
        let text = p.content_changes.first().unwrap().text.clone();
        self.judgement(&text, p.text_document.uri).await;
    }

    async fn completion(&self, _p: CompletionParams) -> Result<Option<CompletionResponse>> {
        return Ok(Some(CompletionResponse::Array(vec![CompletionItem {
            label: "\"   \"".to_owned(),
            label_details: Some(CompletionItemLabelDetails {
                detail: Some("None, null, (), _".to_owned()),
                description: Some("Nothing, Zip, Nada, Nicht".to_owned()),
            }),
            kind: Some(CompletionItemKind::TEXT),
            detail: Some("Nothing".to_owned()),
            documentation: Some(Documentation::String(DOCUMENTATION.to_owned())),
            deprecated: Some(false),
            preselect: Some(false),
            sort_text: Some("".to_owned()),
            filter_text: Some(" ".to_owned()),
            insert_text_mode: Some(InsertTextMode::AS_IS),
            text_edit: Some(CompletionTextEdit::Edit(TextEdit {
                range: Range {
                    start: Position::new(0, 0),
                    end: Position::new(u32::MAX, u32::MAX),
                },
                new_text: "".to_owned(),
            })),
            additional_text_edits: None,
            ..Default::default()
        }])));
    }

    async fn shutdown(&self) -> Result<()> {
        Ok(())
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let stdin = tokio::io::stdin();
    let stdout = tokio::io::stdout();

    let common_resp = RESPONSES
        .iter()
        .max_by_key(|resp| resp.weight)
        .unwrap()
        .msg
        .to_owned();
    let (service, socket) = LspService::new(|client| Backend {
        client,
        current_response: Mutex::new(common_resp),
    });
    Server::new(stdin, stdout, socket).serve(service).await;
}
